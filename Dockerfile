FROM alpine:latest

LABEL maintainer="Stefan Schneider<schneider@hrz.uni-marburg.de>"

# Install OpenLDAP
RUN apk --no-cache add openldap openldap-clients openldap-back-mdb nano

COPY conf/ /etc/openldap/
COPY schema/ /etc/openldap/schema/

RUN chown root:ldap /etc/openldap/slapd.conf \
  && chown root:ldap /etc/openldap/ldap.conf \
  && chmod 640 /etc/openldap/slapd.conf \
  && chmod 640 /etc/openldap/ldap.conf \
  && chmod -R 444 /etc/openldap/schema/*

RUN mkdir -p /run/openldap && mkdir -p /var/lib/openldap/run && chown -R ldap:ldap /run/openldap

EXPOSE 389

# Set default command
CMD ["slapd","-u","ldap","-g","ldap","-h","ldap:/// ldapi:///","-d","4"]
