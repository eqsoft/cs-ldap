#!/usr/bin/env bash
. .env

docker build . -t registry.gitlab.com/eqsoft/cs-ldap:${STACK_ID}
